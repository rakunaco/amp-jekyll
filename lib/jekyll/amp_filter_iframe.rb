require 'nokogiri'
require 'fastimage'

module Jekyll
  module AmpFilter
    # Filter for HTML 'img' elements.
    # Converts elements to 'amp-img' and adds additional attributes
    # Parameters:
    #   input       - the content of the post
    #   responsive  - boolean, whether to add layout=responsive, true by default
    def amp_iframes(input, responsive = true, wi = nil, he = nil)
      doc = Nokogiri::HTML.fragment(input);
      # Add width and height to img elements lacking them
      doc.css('iframe:not([width])').each do |iframe|
        if wi && he
          iframe['width']  = wi
          iframe['height'] = he
        else
          if iframe['src'].start_with?('http://', 'https://')
            src = iframe['src']
          else
            # FastImage doesn't seem to handle local paths when used with Jekyll
            # so let's just force the path
            src = File.join(Dir.pwd, '_site', iframe['src'])
          end
          # Jekyll generates static assets after the build process.
          # This causes problems when trying to determine the dimensions of a locally stored image.
          # For now, the only solution is to skip the build and generate the AMP files after the site has beem successfully built.
          # TODO: find a better solution.
          begin
            size = FastImage.size(src)
            iframe['width']  = size[0]
            iframe['height'] = size[1]
          rescue Exception => e
            puts 'Unable to get image dimensions for "' + src + '". For local files, build the site with \'--skip-initial-build\' for better results. [Error: ' + e.to_s + ']'
          end
        end
      end
      # Change 'img' elements to 'amp-img', add responsive attribute when needed
      doc.css('iframe').each do |iframe|
        iframe.name = "amp-iframe"

        iframe['layout'] = "responsive" if responsive
      end

      # Picture elements are not accepted in amp pages, convert them to amp-img
      #<picture>
      #   <source srcset="mdn-logo-wide.webp" type="image/webp">
      #   <source srcset="mdn-logo-wide.png" media="(min-width: 600px)">
      #   <img src="mdn-logo-narrow.png" alt="MDN">
      #</picture>
      # Move amp-img elements inside picture elements outside of it and remove picture elements
      doc.css('picture').each do |picture|
        # Get img element from picture
        amp_iframe = picture.css('amp-iframe')
        picture.add_next_sibling(amp_iframe) unless amp_iframe.empty?

        # Remove picture element
        picture.remove
      end

      # Added <img /> tag wrapped with <noscript /> in case js is not enabled
      # but image will still show up. The element would look like this:
      # <amp-img ...>
      #    <noscript>
      #        <img ... />
      #    </noscript>
      # </ampimg ...>
      # Duplicate amp-img, remove layout attribut, wrap it with noscript, and add
      # it as amp-img child
      doc.css('amp-iframe').each do |amp_iframe|
        noscript = Nokogiri::XML::Node.new "noscript", doc

        noscript_iframe = amp_iframe.dup
        noscript_iframe.remove_attribute('layout')
        noscript_iframe.remove_attribute('allowfullscreen')
        noscript_iframe.name = 'iframe'
        amp_iframe['sandbox'] = 'allow-scripts allow-same-origin'

        noscript.add_child(noscript_iframe)

        amp_iframe.add_child(noscript)
      end

      # Return the html as plaintext string
      doc.to_s
    end
  end
end

Liquid::Template.register_filter(Jekyll::AmpFilter)
